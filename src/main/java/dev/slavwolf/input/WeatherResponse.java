package dev.slavwolf.input;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.List;

public class WeatherResponse {
    private final Metadata metadata;
    private final List<Item> items;

    @JsonCreator
    public WeatherResponse(Metadata metadata, List<Item> items) {
        this.metadata = metadata;
        this.items = items;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public List<Item> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return "WeatherResponse{" +
                "metadata=" + metadata +
                ", items=" + items +
                '}';
    }
}
