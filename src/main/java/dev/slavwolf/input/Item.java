package dev.slavwolf.input;

import java.util.List;

public class Item {

    private final String timestamp;
    private final List<Reading> readings;

    public Item(String timestamp, List<Reading> readings) {
        this.timestamp = timestamp;
        this.readings = readings;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public List<Reading> getReadings() {
        return readings;
    }
}
