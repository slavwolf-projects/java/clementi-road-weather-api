package dev.slavwolf.input;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.List;

public class Metadata {
    private List<Station> stations;

    @JsonCreator
    public Metadata(List<Station> stations) {
        this.stations = stations;
    }

    public List<Station> getStations() {
        return stations;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "stations=" + stations +
                '}';
    }
}
