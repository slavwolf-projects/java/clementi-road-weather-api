package dev.slavwolf.input;

public class Station {
    private final String id;
    private final String deviceId;
    private final String name;

    public Station(String id, String deviceId, String name) {
        this.id = id;
        this.deviceId = deviceId;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Station{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
