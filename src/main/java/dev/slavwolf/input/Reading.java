package dev.slavwolf.input;

import java.math.BigDecimal;

public class Reading {
    private final String stationId;
    private final BigDecimal value;

    public Reading(String stationId, BigDecimal value) {
        this.stationId = stationId;
        this.value = value;
    }

    public String getStationId() {
        return stationId;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Reading{" +
                "stationId='" + stationId + '\'' +
                ", value=" + value +
                '}';
    }
}
