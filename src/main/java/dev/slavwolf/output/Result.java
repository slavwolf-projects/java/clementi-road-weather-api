package dev.slavwolf.output;

import dev.slavwolf.JsonReader;
import dev.slavwolf.TimedValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Result {
    private final List<String> timestamps;
    private final List<BigDecimal> values;

    public Result(List<String> timestamps, List<BigDecimal> values) {
        this.timestamps = timestamps;
        this.values = values;
    }

    public static Result of(List<TimedValue> timedValues) {
        ArrayList<String> timestamps = new ArrayList<>();
        ArrayList<BigDecimal> values = new ArrayList<>();
        timedValues.forEach(timedValue -> {
            timestamps.add(timedValue.getTimestamp());
            values.add(timedValue.getValue());
        });
        Result result = new Result(timestamps, values);
        return result;
    }

    public List<String> getTimestamps() {
        return timestamps;
    }

    public List<BigDecimal> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "Result{" +
                "timestamps=" + timestamps +
                ", values=" + values +
                '}';
    }
}
