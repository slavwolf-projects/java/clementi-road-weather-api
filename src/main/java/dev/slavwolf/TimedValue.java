package dev.slavwolf;

import java.math.BigDecimal;

public class TimedValue {
    private final String timestamp;
    private final BigDecimal value;

    public TimedValue(String timestamp, BigDecimal value) {
        this.timestamp = timestamp;
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Reading{" +
                "timestamp='" + timestamp + '\'' +
                ", value=" + value +
                '}';
    }
}
