package dev.slavwolf;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import dev.slavwolf.input.WeatherResponse;
import dev.slavwolf.output.Result;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class JsonReader {

    public static final String STATION_NAME = "Clementi Road";

    // ID: S50
    // https://api.data.gov.sg/v1/environment/air-temperature?date=2022-01-06
    public static void main(String[] args) throws IOException {
        args = new String[] { "2021-12-31", "2022-01-06" };
        LocalDate from = LocalDate.parse(args[0]);
        LocalDate to = LocalDate.parse(args[1]);
        ObjectMapper objectMapper = objectMapperForWeatherApi();
        List<TimedValue> timedValues = scrape(from, to, objectMapper);
        objectMapper.writeValue(new File("out.json"), Result.of(timedValues));
        System.out.println("done! <3");
    }

    private static List<TimedValue> scrape(LocalDate from, LocalDate to, ObjectMapper objectMapper) {
        List<TimedValue> timedValues = IntStream
                .iterate(0, n -> n + 1)
                .mapToObj(from::plusDays)
                .takeWhile(nextDate -> nextDate.isBefore(to) || nextDate.isEqual(to))
                .flatMap(nextDate -> {
                    try {
                        System.out.println("fetching " + nextDate + "...");
                        WeatherResponse weatherResponse = fetch(objectMapper, nextDate);
                        String desiredStationId = resolveStationId(nextDate, weatherResponse);
                        return stationValues(weatherResponse, desiredStationId);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .toList();
        return timedValues;
    }

    private static ObjectMapper objectMapperForWeatherApi() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new ParameterNamesModule());
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    private static Stream<TimedValue> stationValues(WeatherResponse weatherResponse, String desiredStationId) {
        return weatherResponse.getItems()
                .stream()
                .flatMap(item ->
                    item.getReadings().stream()
                            .filter(reading -> desiredStationId.equalsIgnoreCase(reading.getStationId()))
                            .findFirst()
                            .map(reading -> new TimedValue(
                                    item.getTimestamp(),
                                    reading.getValue()
                            ))
                            .stream()
                );
    }

    private static String resolveStationId(LocalDate nextDate, WeatherResponse weatherResponse) {
        String desiredStationId = weatherResponse.getMetadata().getStations()
                .stream()
                .filter(station -> STATION_NAME.equalsIgnoreCase(station.getName()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("station %s not found in response for %s".formatted(STATION_NAME, nextDate)))
                .getId();
        return desiredStationId;
    }

    private static WeatherResponse fetch(ObjectMapper objectMapper, LocalDate nextDate) throws IOException {
        WeatherResponse weatherResponse = objectMapper.readValue(
                new URL("https://api.data.gov.sg/v1/environment/air-temperature?date=" + nextDate.toString()),
                WeatherResponse.class
        );
        return weatherResponse;
    }

}
